
## Repositório xx

### Para configurar o repositório localmente utilize os comando abaixo:


* Garanta que os pacotes estejam atualizado executando o comando:

`
npm install
`

Com isso o pacote está instalado corretamente.

****
### Como subir uma nova versão

* Após fazer as alterações necessárias, basta subir a versão do pacote que se encontra no package.json. Para isso utilize o comando npm version, veja  help dele para ver os comandos. Para alterações simples basta utilizar este comando:


`
npm version patch
`
* Agora basta fazer o commit da alteração da versão e criar a pull-request.
* Quando aprovada a pipeline vai atualizar automaticamente a versão do npm.
